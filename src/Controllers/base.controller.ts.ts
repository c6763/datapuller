import * as express from 'express';
 
abstract class BaseController {
  public router = express.Router();

  constructor() {
  }
 
  public abstract intializeRoutes(): void;
}
 
export default BaseController;