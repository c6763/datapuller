import * as express from 'express';
import { RyanairRecordsPuller } from '../DataPullers/RyanairRecordsPuller';
import { WizzairRecordsPuller } from '../DataPullers/WizzairRecordsPuller';
import BaseController from './base.controller.ts';

class DestinationsController extends BaseController {
  public path = '/destinations';

  constructor() {
    super();
    this.intializeRoutes();
  }

  public intializeRoutes(): void {
    this.router.get(this.path, this.getDestinations);
  }


  getDestinations = async (request: express.Request, response: express.Response) => {
    try {
      let destinations = [];
      let wizz = new WizzairRecordsPuller();
      let ryan = new RyanairRecordsPuller();
      destinations.push(...(await wizz.pullRecords()));
      destinations.push(...(await ryan.pullRecords()));
      console.log(destinations);
      response.status(200).send(destinations);
    } catch (error: any) {
      response.status(500).send(error.message);
    }
  }
}

export default DestinationsController;