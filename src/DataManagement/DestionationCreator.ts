import { CurrencyType } from "../Enums/CurrencyType";
import { Direction } from "../Enums/Direction";
import { Month } from "../Enums/Month";
import { Website } from "../Enums/WebSite";
import { Destination } from "../Objects/Destination";

export class DestinationCreator {
    public static CreateDestination(cityName: string, priceTxt: string, currency: CurrencyType, monthTxt: string, website: Website, direction: Direction): Destination {
        const purePrice = this.GetNumberFromText(priceTxt);
        const pureMonth = this.GetMonthFromText(monthTxt);
        const pureCityName = cityName.replace("\n", "").replace("\r", "").trim();
        return new Destination(pureCityName, purePrice, currency, pureMonth, website, direction);
    }

    private static GetMonthFromText(monthTxt: string): Month {
        const months = Object.keys(Month).filter((monthKey) => isNaN(Number(monthKey)));
        for (let i = 1; i < months.length; i++) {
          if (monthTxt.indexOf(months[i]) != -1) {
            return <Month>i;
          }
        }
        return Month.Undefined;
      }
  
      private static GetNumberFromText(txt: string): number {
        let number = 0;
        for (let i = 0; i < txt.length; i++) {
          if (txt[i] >= '0' && txt[i] <= '9') {
            number *= 10;
            number += Number(txt[i]);
          }
        }
        return number;
      }
}
