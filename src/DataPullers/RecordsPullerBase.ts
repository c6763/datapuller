import { Browser, ElementHandle, Page } from "puppeteer"
import puppeteer from "puppeteer-extra"
import { Destination } from "../Objects/Destination";

export abstract class RecordsPullerBase {
   protected browserPage!: Page;
   private browser!: Browser;

   public abstract pullRecords(): Promise<Destination[]>;

   protected async openBrowser() {
    this.browser = await puppeteer.launch({
      headless: true,
      ignoreHTTPSErrors: true,
      slowMo: 0,
      args: ['--disable-gpu', "--disable-features=IsolateOrigins,site-per-process", '--blink-settings=imagesEnabled=true']})
    
    this.browserPage = await this.browser.newPage();
   }

   protected async closeBrowser() {
     await this.browser.close();
   }

   protected async getSelector(selector: string): Promise<ElementHandle<Element>>{
      await this.browserPage.waitForSelector(selector)
      return <ElementHandle<Element>>(await this.browserPage.$(selector));
    }
    
    protected async tryClickOnBtn(selector: string): Promise<void> {
      try{await this.browserPage.click(selector)}catch{}
    }

    /*
    click on btn several times
    */
    protected async tryClickOnBtnLoop(selector: string, numOfClicks: number): Promise<void> {
      while (numOfClicks > 0) {
        try{
          await this.tryClickOnBtn(selector);
          numOfClicks--;
        }catch{}
        await this.delayBrowser(100);
      }
    }

    protected async clickOnBtn(selector: string): Promise<void> {
    let button = await this.getSelector(selector);
    await button.click();
   }

   protected async getDataBySelector(selector: string): Promise<string> {
      let element = await this.getSelector(selector);
      return await this.browserPage.evaluate(el => el.textContent, element)
    }

    protected async delayBrowser(delayMs: number): Promise<void> {
      await this.browserPage.waitFor(delayMs);
    }

   protected async goToPage(page_URL: string): Promise<void>{
    while (true) {
      try{
        await this.browserPage.goto(page_URL, { waitUntil: 'networkidle2', timeout: 5000 });
        break;
      } catch {
        console.log(`Error in loading page, re-trying...`)
      }
    }
    
  }
}
 
