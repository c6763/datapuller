import puppeteer from "puppeteer-extra"
import StealthPlugin from "puppeteer-extra-plugin-stealth"
import { RecordsPullerBase } from "./RecordsPullerBase"
import { CurrencyType } from "../Enums/CurrencyType"
import { Destination } from "../Objects/Destination"
import { DestinationCreator } from "../DataManagement/DestionationCreator"
import { Website } from "../Enums/WebSite"
import { Direction } from "../Enums/Direction"

puppeteer.use(StealthPlugin())

export class RyanairRecordsPuller extends RecordsPullerBase {

    constructor() {
      super();
    }
    public async pullRecords(): Promise<Destination[]> {
        let destinations: Destination[] = [];
        await this.openBrowser();
        await this.goToPage('https://www.ryanair.com/gb/en/cheap-flights')
        //accept cookies
        await this.tryClickOnBtn("#cookie-popup-with-overlay > div > div.cookie-popup-with-overlay__buttons > button.cookie-popup-with-overlay__button");
        //move to other page
        await this.tryClickOnBtn('body > div.FR > main > div > div.header-container > div.fare-finder-search-container > form > div.lets-go > button');
        await this.browserPage.waitForNavigation();
        destinations.push(...(await this.readDestinationsFromPage(true)));
        //try Move to next page
        await this.tryClickOnBtnLoop('body > div.FR > main > div > div.container.results-container > farefinder-widget > div > div.inner-wrapper > div:nth-child(2) > div > div > core-pagination > div > div.next.vertical-center', 1);
        await this.delayBrowser(1000);
        destinations.push(...(await this.readDestinationsFromPage(false)));
        await this.closeBrowser();
        return destinations;
      }

    private async getSecondPageRange(): Promise<number> {
      let content = await this.getDataBySelector("body > div.FR > main > div > div.container.results-container > farefinder-widget > div > div.inner-wrapper > div:nth-child(2) > div > div > core-pagination > div > ul > li:nth-child(2) > a");
      return (+content.split('-')[1]) - 16;
    }

    private async readDestinationsFromPage(isFirstPage: boolean): Promise<Destination[]> {
      let destinations: Destination[] = [];
      let range = (isFirstPage)? 16: await this.getSecondPageRange();
      for (let i = 1; i <= range; i++) {
        const cityNameSelector = `body > div.FR > main > div > div.container.results-container > farefinder-widget > div > div.inner-wrapper > div:nth-child(2) > div > div > div > div > div > div > div:nth-child(${i}) > div > div.ff-city-list > span.airport`;
        const cityNameData = await this.getDataBySelector(cityNameSelector);
        const priceSelector = `body > div.FR > main > div > div.container.results-container > farefinder-widget > div > div.inner-wrapper > div:nth-child(2) > div > div > div > div > div > div > div:nth-child(${i}) > div > div.ff-price-type > div.price > span.price-units`;
        const priceData = await this.getDataBySelector(priceSelector);
        const monthSelector = `body > div.FR > main > div > div.container.results-container > farefinder-widget > div > div.inner-wrapper > div:nth-child(2) > div > div > div > div > div > div > div:nth-child(${i}) > div > div.ff-text-month > span:nth-child(2)`;
        const monthData = await this.getDataBySelector(monthSelector);
        destinations.push(DestinationCreator.CreateDestination(cityNameData, priceData, CurrencyType.Euro, monthData, Website.Ryanair, Direction.OneWay));
      }
      return destinations;
    }
}
 
