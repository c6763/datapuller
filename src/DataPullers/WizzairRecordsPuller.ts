import puppeteer from "puppeteer-extra"
import StealthPlugin from "puppeteer-extra-plugin-stealth"
import { Page } from "puppeteer"
import { RecordsPullerBase } from "./RecordsPullerBase"
import { CurrencyType } from "../Enums/CurrencyType"
import { Destination } from "../Objects/Destination"
import { DestinationCreator } from "../DataManagement/DestionationCreator"
import { Website } from "../Enums/WebSite"
import { Direction } from "../Enums/Direction"

puppeteer.use(StealthPlugin())

export class WizzairRecordsPuller extends RecordsPullerBase {
    private PRICES_SELECTOR = "#app > div > main > div > div > div > div.trip-planner__content > div > div.trip-planner__content__results > article > div > div.trip-planner-card__flight-details__meta > h4";
    private CITY_NAMES_SELECTOR = "#app > div > main > div > div > div > div.trip-planner__content > div > div.trip-planner__content__results > article > header > div.trip-planner-card__container > h1";
    private MONTHS_SELECTOR = "#app > div > main > div > div > div > div.trip-planner__content > div > div.trip-planner__content__results > article > div > div.trip-planner-card__flight-details__meta > small:nth-child(3)";

    constructor() {
      super();
    }
    public async pullRecords(): Promise<Destination[]> {
        let destinations: Destination[] = [];
        await this.openBrowser();
        await this.goToPage('https://wizzair.com/en-gb/flights/trip-planner#/TLV')
        //pass bot detection
        await this.tryClickOnBtnLoop('.modal__close', 3);
        //click on expand btn
        await this.clickOnBtn('#app > div > main > div > div > div > div.trip-planner__content > div > div.trip-planner__content__footer > button');
        
        destinations.push(...(await this.readDestinationsFromPage()));
        await this.closeBrowser();
        return destinations;
      }

      private async readDestinationsFromPage(): Promise<Destination[]> {
        let destinations: Destination[] = [];
        const dataSelectors = [ this.PRICES_SELECTOR, this.CITY_NAMES_SELECTOR, this.MONTHS_SELECTOR ];
        const dataResults = await this.getDataBySelectors(dataSelectors);
        let prices = dataResults[0];
        let cityNames = dataResults[1];
        let months = dataResults[2];
        for(let i = 0; i < prices.length; i++) {
          destinations.push(DestinationCreator.CreateDestination(cityNames[i], prices[i], CurrencyType.Shekel, months[i], Website.Wizzair, Direction.TwoWay));
        }
        return destinations;
      }

      private async getDataBySelectors(dataSelectors: string[]): Promise<string[][]> {
        let result = await this.browserPage.evaluate((selectors) => {
            let dataArrays: string[][] = [];
            selectors.forEach((dataSelector: string) => {
                dataArrays.push(<string[]>Array.from(document.querySelectorAll(dataSelector)).map(x => x.textContent))
            });
            return dataArrays;
        }, dataSelectors)
        return <string[][]>result;
      }
}
 
