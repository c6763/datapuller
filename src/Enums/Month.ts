export enum Month {
    Undefined = -1, January = 0, February = 1, March = 2, April = 3, May = 4, June = 5, July = 6, August = 7, September = 8, October = 9, November = 10, December = 11
}

export function GetHebrewMonthsMap() {
    let monthsMap = new Map<Month, string>();
    monthsMap.set(Month.January, "ינואר");
    monthsMap.set(Month.February, "פברואר");
    monthsMap.set(Month.March, "מרץ");
    monthsMap.set(Month.April, "אפריל");
    monthsMap.set(Month.May, "מאי");
    monthsMap.set(Month.June, "יוני");
    monthsMap.set(Month.July, "יולי");
    monthsMap.set(Month.August, "אוגוסט");
    monthsMap.set(Month.September, "ספטמבר");
    monthsMap.set(Month.October, "אוקטובר");
    monthsMap.set(Month.November, "נובמבר");
    monthsMap.set(Month.December, "דצמבר");
    return monthsMap;
}