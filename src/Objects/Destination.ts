import { CurrencyType } from "../Enums/CurrencyType";
import { Direction } from "../Enums/Direction";
import { Month } from "../Enums/Month";
import { Website } from "../Enums/WebSite";

export class Destination {
  cityName: string;
  price: number;
  currency: CurrencyType;
  month: Month;
  website: Website;
  direction: Direction;
  constructor(cityName: string, price: number, currency: CurrencyType, month: Month, website: Website, direction: Direction) {
    this.cityName = cityName;
    this.price = price;
    this.currency = currency;
    this.month = month;
    this.website = website;
    this.direction = direction;
  }
}