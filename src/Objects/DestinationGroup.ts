import { TranslationManager } from "../DataManagement/TranslationManager";
import { CurrencyType } from "../Enums/CurrencyType";
import { Month } from "../Enums/Month";
import { Website } from "../Enums/WebSite";
import { Destination } from "./Destination";

export class DestinationGroup {
  destinations: Destination[];
  constructor(destinations: Destination[]) {
    this.destinations = destinations;
  }

  public ToString(): string {
    let text = "";
    this.destinations.sort((destination1, destination2) => destination1.website - destination2.website);
    let lastDestType = Website.Undefined;
    for (let i = 0; i < this.destinations.length; i++) {
      const currentDestType = this.destinations[i].website;
      if (lastDestType != currentDestType) {
        text += `\n*${Website[currentDestType]}*`;
        lastDestType = currentDestType;
      }
      text += `\n${i + 1}. ${TranslationManager.GetInstance().GetTranslation(this.destinations[i].cityName)} - ${this.destinations[i].currency}${this.destinations[i].price} ${this.destinations[i].direction}`;
    }
    return text;
  }
}