export class Translation {
  public name: string;
  public translatedName: string;

  constructor(name: string, translatedName: string) {
    this.name = name;
    this.translatedName = translatedName;
  }
}