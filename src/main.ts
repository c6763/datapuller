import App from './app';
import DestinationsController from "./Controllers/destinations.controller";

const app = new App(
    [
      new DestinationsController()
    ],
    5001,
  );
   
  app.listen();